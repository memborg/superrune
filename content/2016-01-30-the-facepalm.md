+++
title = "The facepalm"
description = "You may know the Football World Cup logo Brazil 2014 looking like a facepalm."
template = "page.html"
date = 2016-01-30

[taxonomies]
categories = ["sport"]
tags = ["funny", "logo"]

[extra]
image = "images/2014-world-cup-logo.webp"
+++

You may know the Football World Cup logo Brazil 2014 looking like a facepalm
and thought to yourself that looked pretty funny.
<!-- more -->

![The World Cup Brazil 2014 logo](/images/fifa-brazil-2014-comic.webp)

Well it turns out that the new Olympic Games Rio 2016 logo are also something
else entirely. You might think this look like a small group of people holding
hands in a odd shaped figure, but as my daugter pointed out it actually looks
like a pacifier.

![The Olympic Games Rio 2016 Pacifier](/images/Rio 2016.webp)

There you have it, the Brazillians seems to be fond of having their logos
showing you different images depending on the eye of the beholder.

