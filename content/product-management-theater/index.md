+++
title = "Product management theatre"
description = "Today's Product Managers are under pressure from AI and has to redefine their role and become real product managers"
date = 2024-03-13

[taxonomies]
categories = ["agile"]
tags = ["svpg", "podcast", "product management"]

[extra]
image = "product-management-theatre.webp"
+++

Today's Product Managers are under pressure from AI and has to redefine their
role and become real product managers

On [Lenny's podcast](https://www.youtube.com/watch?v=9N4ZgNaWvI0) Marty Cagan
talks about how most of today's product manager in reality is not product
managers, but project managers, backlog administrators, products owners and so.

If you are one of those "product managers" you should according to Marty think
hard about the job you have.

<!-- more -->

[The Beautiful Mess](https://cutlefish.substack.com/) by John Cutler has taken
[notes](https://www.linkedin.com/pulse/notes-product-management-theater-marty-cagan-lennys-podcast-cutler-yuzzc/)
about the podcast and has shared his view on the statements from Marty Cagan.
It offers another perspective on the whole product manager theatre.
