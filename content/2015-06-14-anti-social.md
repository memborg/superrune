+++
title = "Anti-social"
description = "I've cleaned up some of my accounts an this time I quitted Twitter, Instagram and Google"
template = "page.html"
date = 2015-06-14

[taxonomies]
categories = [ "socialmedia" ]
tags = ["quitting", "socialmedia"]
+++

I've cleaned up some of my accounts an this time I quitted Twitter, Instagram
and Google. I've been using these platforms for some times now and I came to
realize that they weren't giving me anything at all.
<!-- more -->

## Instagram

This was the most recently social platform I joined and I think I've been using
it for 6-8 months. I joined the service primaryly to see photos from F1, my
sisters kids and following up newborn baby animals in the local zoo. I posted
a few images of food, 'cause you got to and places I went, but a time flew by
and if followed more and more people and ended up feeling like a stalker.  it
were the same reason I quit Facebook as I'm now quitting instagram. I'm not
getting anything real from using instagram it is just a bunch of broadcats frm
here to there and it is only thrilling the gossip part of my brain. So TO HELL
WITH IT.
  
## Twitter

When I first joined twitter in 2009 it waw exciting. 140 characters limit and
what not. I dumped all my RSS feed because Google was closing down their reader
app. At one point I used the reader more than my mail. I created lists with the
same topics as I were doing for mig RSS feed and bought the Tweetbot. It was
the only real twitter app at the time.  I got hooked on justunfollow to see who
has been unfollowing me late so I could do the same to them back. I got
concerned about my Klout Score! and I spent a great deal of time on twitter and
tried to get into conversations with others, but the limit of 140 characters
always hit me.  The one day I read this blog post [I Hope Twitter Goes
Away](https://alexgaynor.net/2014/oct/30/i-hope-twitter-goes-away/) by Alex
Gaynor and it struck me that is what I've been struggling with too.  So a few
days ago i decided to go through all the persons I was following and pick those
I would like to read a blog post from, find their RSS or Atom feed and via
[IFTTT](https://ifttt.com) create a recipe to send me an e-mail everytime they
post something.  Then I downloaded all my tweets and pressed delete on my
account with a little help from [Just Delete Me](http://justdelete.me).
  
## Google

Now the final big one. A few years back i migrated all my mail to Outlook.com
because I was getting tired of GMail. I dumped Google Search in favor of
[DuckDuckGo](https://duckduckgo.com) and have never looked back. Yes there are
occasion were I need to use google, but the very seldom. My wife and I dumped
Google Calendar in favor of icloud calendars because Google accounts was
a growing pain on iOS. Mail push stopped working, Google dropped ActiveSync in
favor of their own. Google Calendar had sync problems and we would end up
missing appointments.  Then the tinfoil hat suddently appeared on my head an
Google became this evil spying company which cannot be trusted and sells my
information to the lowest bidder. That was one part on my frustration, the
other part was that Google kept killing of the services I used or stopped
maintaining them.  So after a few years not using my Google account I decided
to end my connection to Google.  Yes I know I'm using more services now, but at
least I'm not depended on one provider of e-mail, calendar and search. Google
does create cool stuff and but they still seem all over the board with new
inventions and keeps forgetting things they build a long time ago. In my
opionen GMail need a face lift as does calendar, but it looks like Google has
forgotten the desktop in favor of Android.
