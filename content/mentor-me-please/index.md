+++
title = "Mentor me, please!"
description = "I have always thought mentoring would be valuable, but I have never thought about in what context or even timeframe."
date = 2024-04-15

[taxonomies]
categories = ["people"]
tags = ["montor", "mentoree", "mentoring", "podcast"]

[extra]
image = "mentor-me-please.webp"

+++

I have always thought mentoring would be valuable, but I have never thought
about in what context or even time frame.

<!-- more -->

I recently listened to [Books, Remote Work, and Q&A, with Adrienne Tacke](https://www.youtube.com/watch?v=Ac1B4YP9FkY) podcast where Adrienne gets
a question about mentoring and not getting a lot of responses on ones request.

She has some good points on setting the right expectations when seeking a mentor.

She of course expects that you have done the needed research about the person
you would like to mentor you.

But then she lists other things that one should consider when asking for
a mentor, that when she says it out loud makes perfect sense, but you do not
have given it much thought.

* Why are you seeking a mentor?
* What kind of mentorship?
	* Reviewing of something
	* Career goals or advancement
	* Getting into the industry
* What is the time frame? Weeks, months or years?
* How often do you meet and for how long? Once a week for an hour?
* Is there a need for asking questions in between meeting?
* Is this just some advice you are looking for on a single topic or question?

There are probably other things to consider, and have a listen to the podcast
for Adriennes words on the topic.

