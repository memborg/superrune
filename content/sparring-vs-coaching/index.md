+++
title = "Sparring vs Coaching"
description = "Seek out a sparring partner being in the same situation to improve your skills"
date = "2023-11-13"

[taxonomies]
categories = ["development"]
tags = ["sparring", "coaching", "skills"]

[extra]
image = "sparring.webp"
+++

In the podcast [10 lessons on career, growth, and life](https://www.youtube.com/watch?v=ZG3iNH4vvMA) Brian Balfour has a nice comment about sparring versus coaching.

I'm paraphrasing it a bit but you'll get the idea

> Have a sparring partner to get better. Coaching will only take you so far.
> Your sparring partner is in the arena with you and can give you timely
> feedback.

<!-- more -->
