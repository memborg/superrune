+++
title = "Flyttet til Netlify"
description = "Ny hosting udbyder - Netlify.com"
template = "page.html"
date = 2017-10-28

[taxonomies]
categories = [ "hosting" ]
+++

Mit site er nu flyttet til [Netlify](http://netlify.com), som bygger min
[JekyllRB](http://jekyllrb.com) løsning via CI/CD og hoster den der. Der er
indbygget support for [Let's Encrypt](Https://letsencrypt.org), så må vi se
hvordan det fungere. Manuel vedligehold på egen maskine er for bøvlet.

<!-- more -->
