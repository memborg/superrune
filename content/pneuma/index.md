+++
title = "Pneuma"
description = "Lidt om hvordan jeg kom til at høre Tool"

date = 2022-04-23

[taxonomies]
categories = ["music"]
tags = ["metal","tool"]
+++

Jeg har over den seneste tid hørt utroligt meget Tool. Det er musik jeg ikke har hørt før og det er noget helt andet end alt det andet jeg har hørt.

<!-- more -->

Det kom sig af at jeg høre Sort Søndag podcasten med Anders Bøtter. I baggrunden køre der lidt musik mens der bliver talt og jeg var sikker på at det var et Tool nummer uden helt at vide det.

Jeg tænkte at jeg nok burde høre deres Tool special for det kan jo være jeg finder ud af hvad kunnetet hedder. Og ganske rigtigt dukker lyden op igen ca. midt i podcasten. Det viser sig at være nummeret Pneuma fra albummet Fear Inoculum (2019) og det sprang næsten mit hoved i luften med vanvittige trommer og guitar riffs. Og selvom nummeret er over 13 minutter er det slet ikke for langt eller trættende. Jeg sidder og ønsker mere og starter nummeret forfra når det er slut. Omkring 2/3 henne i nummeret sker noget jeg ikke kan sætte ord på, men jeg kan ikke lade være med at få et kæmpe smil på. Så fedt er nummeret for mig.

Men efter at hørt Tool special er der endnu flere numre jeg synes er fede. Sober fra debut albummet Undertow (1993) er pisse godt.

Nu starter jeg fra enden af og høre alle Tools albums igennem fra ende til anden. Det gode er at det ikke behøver at ske lige nu og her. Det tog dem 13 år at producere Fear Inoculum og selvom der nok ikke går så mange år før næste album kommer, så går der hvertfald nogle år uden næste album.

Forvent at når du møder mig at jeg godt kan finde på at bringe Tool ind i samtalen.

Tak til Sort Søndag for at udvide min musik horisont endnu mere.
