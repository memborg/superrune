+++
# Template to use to render this page
template = "nonpost.html"
path = "hello"

title = "Hello"

description = "Hello, I'm Rune Memborg. Here is how I prefer to keep in touch, and why"

+++

Hello, I'm Rune Memborg.

Here is how I prefer to keep in touch, and why:

<ul class="list">
	<li>My favourite way to converse is email &rarr; <object class="svg-email-protection" data="/images/email.svg" type="image/svg+xml"></object>.</li>
	<li>I mainly write on my <a href="https://superrune.dk">website</a></li>
	<li>I dislike <a href="https://www.linkedin.com/in/memborg/" rel="me">LinkedIn</a>  as a platform, but use it to connect with work colleagues, and to keep an eye on the job market. I would prefer communication on e-mail instead of LinkedIn.</li>
	<li class="item">You can join me on of my <a href="https://gitlab.com/memborg" rel="me">GitLab projects.</a></li>
	<li>Find me on <a href="https://m.me/superrunedk">FB Messenger</a> for chats. I still prefer e-mail.</li>
	<li class="item">Maybe Whatsapp is worth a try. <br/><img width="250" height="250" style="width: 250px;" src="/images/whatsapp.webp" alt="WhatsApp Superrune QR code"/></li>
</ul>
