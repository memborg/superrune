+++
template = "nonpost.html"
path = "later"
title = "Read it later"
description = "A longer list of items I want to read at some point"
+++

A longer list of items I want to read at some point.

It is ordered by added date and when I am done reading, it will be deleted from the list.

## The list

* [Improving Team Morale is not an Objective](https://marcgg.com/blog/2025/02/19/team-morale/)
* [Fixing Full-Stack Teams - by Doc Norton - Doc’s Substack](https://docondev.substack.com/p/fixing-full-stack-teams)
* [Measuring developer productivity: A clear-eyed view](https://newsletter.getdx.com/p/developer-productivity-metrics-the)
* [High Ownership, High Urgency | blog.danielna.com](https://blog.danielna.com/high-ownership-high-urgency/)
* [Weak engineering managers | sean goedecke](https://www.seangoedecke.com/weak-managers/)
* [7 things all introverted leaders should know - Big Think](https://bigthink.com/business/7-things-all-introverted-leaders-should-know/)
* [Hands-On - by Chaitali Narla - ChaiTime](https://www.chaitime.chaitalinarla.com/p/hands-on)
* [The 5 Most Difficult Employees (And How To Actually Handle Them)](https://newsletter.canopy.is/p/the-5-most-difficult-employees-and)
* [Here’s Why Wearing Many Hats is Important for a Software Developer | by Giftmugweni | Curiosity Sparks | Feb, 2025 | Medium](https://medium.com/curiosity-sparks/heres-why-wearing-many-hats-is-important-for-a-software-developer-c27bf24737a4)
* ["You need to be more strategic" — a primer on strategy for software engineers | Writing by Dan Pupius](https://writing.pupius.co.uk/you-need-to-be-more-strategic-dc473a2aa319)
* [Why some software development tasks feel impossible](https://jonhilton.net/friction/)
* [Creating a Sense of Stability - by Claire Lew](https://newsletter.canopy.is/p/creating-a-sense-of-stability)

## To preserve and archive

* <https://brooksreview.net/2024/08/tips-on-using-llms-ai-effectively-for-text/>
* <https://www.getharvest.com/blog/boost-the-planning-process-by-asking-your-team-these-powerful-questions>
* <https://www.itprotoday.com/unified-communications/how-can-i-stop-feeling-invisible-while-working-remotely>
* [How the Three Whats Invite People to a Human Connection - Johanna Rothman](https://www.jrothman.com/mpd/2025/01/how-the-three-whats-invite-people-to-a-human-connection/)
* [A Toolbox for High-Performance Teams | Human-Centered Change and Innovation](https://bradenkelley.com/2025/01/a-toolbox-for-high-performance-teams/)

## Add to my projects

* [ai.robots.txt](https://github.com/ai-robots-txt/ai.robots.txt) How to auto include in existing robots.txt
* [Great Things About Rust Beyond Performance](https://ntietz.com/blog/great-things-about-rust-beyond-perf/). Include the lint option in my Rust projects

## Something to consider

* [Gather, decide, execute: reflecting on my daily system - The Engineering Manager](https://www.theengineeringmanager.com/growth/gather-decide-execute-reflecting-on-my-daily-system/?_bhlid=404c656fc450bb8cf1b1c1e5664659f94b20fc25)


## Blog subjects

* [Roblox says employees must return to office because the metaverse still isn’t good enough - The Verge](https://www.theverge.com/2023/10/17/23921001/roblox-employees-return-to-office-metaverse-isnt-good-enough)
* [Software is Complicated, Given Enough Time – Jessitron](https://jessitron.com/2023/10/26/software-is-complicated-given-enough-time/)
* [10 hard-to-swallow truths they won't tell you about software engineer job](https://www.mensurdurakovic.com/hard-to-swallow-truths-they-wont-tell-you-about-software-engineer-job/)
* [The Bluffer’s Guide to The Mythical Man-Month – Codemanship's Blog](https://codemanship.wordpress.com/2023/11/20/the-bluffers-guide-to-the-mythical-man-month/)
* [Becoming An Engineering Manager Can Make You Better At Life And Relationships – charity.wtf](https://charity.wtf/2023/12/15/why-should-you-or-anyone-become-an-engineering-manager/)
* [Burnout - When does work start feeling pointless? | DW Documentary - YouTube](https://www.youtube.com/watch?v=raVms8w61No)
* [[ Definition of “workflow” in Kanban] | [Orderly Disruption]](https://www.scrum.org/resources/blog/definition-workflow-kanban)
* [Don’t Sell Ideas – Debate Them | tig.log](https://blog.kindel.com/2023/12/17/dont-sell-ideas-debate-them/)
* [Nick Cave - The Red Hand Files - Issue #260 - I’m 17 years old, and I don’t know if I want to record and publish an ep that I’ve been working on with a band with some friends of mine and that I’m very proud of for what regards the instrumental part but I know that my singing could be better and that I could wait a little bit more to create something I’m 100% proud of, but still I don’t want to wait anymore because we have the chance of recording in a great studio and I don’t want to slow down the process. What should I do? : The Red Hand Files](https://www.theredhandfiles.com/singing-could-be-better/)
