+++
# Template to use to render this page
template = "nonpost.html"
path = "about"

title = "About a brilliant man; Rune Memborg"

description = "A brilliant man; Rune Memborg. I'm passioned about software and the creative process of finding a good solution to my problems."

+++

## About a brilliant man

This blog is mostly for my own sake. I write when I have something important to
tell or just to get some thoughts out of my head so I can focus on other
things.

I try to write in English to get better at it and my work of line everything is
in English anyway, so it feel right to do it in English and not i Danish which
is my main language. I will be posting about programming, code snippet, ideas,
projects I’ve been involved in and so on.

### Personal note

There will no be any personnel stuff about me and my family. That I leave to my
wife and my private journal. I will leave you with one private note, though. If
you wonder what the header background is on almost all my profiles, I can tell
you that it’s the symbol the [Danish Sclerosis Association](http://scleroseforeningen.dk/)
is using as its logo and I’m a member because my wife has sclerosis.
