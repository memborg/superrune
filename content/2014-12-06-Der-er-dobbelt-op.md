+++
title = "Dobbelt op på sagerne"
description = "Jeg er kommet lidt bagud med ølsmagningen, derfor dette lille opsamlingsheat med en tripel og noget julet"
template = "page.html"
date = 2014-12-06

[taxonomies]
categories = [ "beer" ]
tags = ["belgian", "beer", "4star"]

[extra]
image = "images/gounden-bush.webp"
+++

Jeg er kommet lidt bagud med ølsmagningen, derfor dette lille opsamlingsheat
tripel og noget julet.
<!-- more -->

Jeg måtte desværre undvære pakken d. 5. december da jeg ikke havde mulighed for
at åbne den, da jeg skulle til firma julefrokost. Der vare rigeligt med andre
juleøl, så jeg gik ikke ned på det.

Jeg skulle have haft Gouden Caroluse Triple om fredagen. Det er en typisk
belgisk øl på 9% og passer fint til mad, som den blev nydt til d. 6. december.
Skal jeg give den stjerner, bliver det 3,5 stjerner. Den er lidt over middel.

Senere på aften åbnede jeg Bush de Noël, igen en hygge øl. 12% for en øl er
lidt magisk, der er et eller andet jeg ikke helt kan sætte finger på. Øl
i dette leje synes jeg gør sig godt, dog har jeg ikke endnu haft en dårlig
oplevelse med disse stærke øl.

Det bliver til 4,9 stjerner for denne øl.

![Gounden Carolus Triple og Bush de Noël](/images/goudencarolus-bushdenoel.webp)
