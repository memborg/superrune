+++
title = "Reading Every Day"
description = "I've set a goal that have I should read 10 minutes in a book every day. It it not much but it all counts"
date = "2023-03-23"

[taxonomies]
categories = ["reading"]

[extra]
image = "current-reading-progress.webp"

+++

I've set a goal that have I should read 10 minutes in a book every day. It it not much but it all counts.

<!-- more -->

I read a lot every day but I don't read many books and I wanted to read more books.

The reason to read more books is to have another source and ressource for all the knowledge I consume but I also want to add more fun into my reading. And to achieve that I will read more fiction.

I have also decided on some reading strategies.

1. I do not need to read all pages in a book. It is OK to skip pages or even
   chapters if they are boring.
2. I do not need to complete the book if it not for me and I will remove it
   from my reading list.

These two rules applies to both fiction and non-fiction. One of the goals is to
read more fiction than non-fiction because I consume non-fiction all day and
this would be a nice break.
