+++
title = "Using ClickATell to send unicode SMS"
description = "Sending a SMS and get it displayed correctly on the device shouldn't be a concern for the end programmer when using a SMS gateway"
template = "page.html"
date = 2014-12-23

[taxonomies]
categories = [ "programming" ]
tags = ["api", "sms", "clickatell"]
+++

Sending a SMS and get it displayed correctly on the device shouldn't be
a concern for the end programmer when using a SMS gateway, but it is. When
using [ClickATell's API](https://www.clickatell.com/apis-scripts/apis/http-s/)
you have to tell it that you are sending unicode characters and encode it in
HEX too.

<!-- more -->

Why you ask? Because some of the oldest network operators are still using old
systems to handle SMS and those systems cannot handle unicode out of the box.
And the there pricing issue. A unicode in it's nature takes up more bytes than
fx. latin1 does so it would require a few more concatnated SMS's to send
unicode than plain latin1. And again this is something the endprogrammer has to
worry about.

## The how

As I mentioned you have to explicit tell ClickATell to send an unicode SMS and
then HEX encode it. Below are the methods I've used in C# to do the encoding.

```cs
private string ToUnicode(string val)
{
    Encoding utf8 = Encoding.UTF8;
    Encoding unicode = Encoding.Unicode;

    byte[] utf8Bytes = utf8.GetBytes(val);

    byte[] unicodeBytes = Encoding.Convert(utf8, unicode, utf8Bytes);

    var result = ByteArrayToString(unicodeBytes);
    result = result.Substring(result.Length - 2, 2) + result.Substring(0, result.Length - 2);
    return result;
}

public static string ByteArrayToString(byte[] ba)
{
    StringBuilder hex = new StringBuilder(ba.Length * 2);
    foreach (byte b in ba)
        hex.AppendFormat("{0:x2}", b);
    return hex.ToString();
}
```

## Ending words

This is not me trying to bad mouth ClickATell the good and reliable service it
is, but merely a display of bad API design. It is also ment to help others in
the same situation where some operators can easily receive sms in unicode
without any transformation and others can't and you have spent too much time on
Google to resolve your problem.
