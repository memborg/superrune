+++

title = "New Insights on Work"
description = "I’ve been getting new insights lately on how we do work from a few sources and I can see how we work today is inefficient."

[taxonomies]
categories = ["work"]
tags = ["TIL","teal","git", "processes"]


+++
I’ve been getting new insights lately on how we do work from a few sources and I can see how we work today is inefficient. 

<!-- more --> 

## Empowerment 

I’ve been reading [Reinventing Organizations](https://www.reinventingorganizations.com/) by Frederic Laloux and I can see how the command-control structure of an organization is inefficient because you need to go up in the hierarchy and ask for permission every time you want something. You cannot just go out and do it you have to ask. It could be you wanted a new mouse for your workstation or that you want to ensure higher quality in the product you are delivering. In all cases you have to ask. In some cases you can make the decision, but typically you’ve to ask for permission. 

My favorite example in the book is where the opposite is true. All organizations described in the book are doing self-organization to the extreme. One of these organizations are making spare parts for the car industry and one day a worker discovers an error in the parts he is producing. He halts the production of this part and goes through all the parts that has been produced and are in storage. He finds one more part has the same error. One would think that this would be enough quality control, but no. He decides to check where all the parts have been sent and decide to drive 8 hours to the car manufacture who received the parts and do a quality check at the car factory too. He discovers that the only two parts that had an error where the ones still in storage and no other had reach the customer yet. He didn’t have to ask for permission go on this trip and could decide what is best for the company in that moment. Had he been in a traditional command-control organization he would probably get a no and some kind of damage control would be set in motion to protect the company from law suits or the likes. No such thing happened and the car manufacture appreciated the extra effort they received by their supplier. This kind of dedication is what we all ask for, but the hierarchy and the lack of trust in the individual hinders it. 

I like this quote by Howard Behar former president of Starbuck: ["The person who sweeps the floor should choose the broom."](https://www.forwardthinkingworkplaces.com/person-sweeps-floor-chooses-broom/). As a worker you should at least have this kind of power.  

## Doing actual work 

On the work level we also have efficiencies on my line of business as [this series of articles are pointing out](https://thinkinglabs.io/articles/2021/04/26/on-the-evilness-of-feature-branching.html). As a software we produce a lot of code and to keep track on changes we use a version control system and in this case Git. It could be any version control system really but the industry standard at this moment is Git. And as anyone else in the industry we have a main branch and feature branches to control what is stable and what is worked on right now. When a feature branch is done by the developer it is merge int the main branch and this is where the inefficiencies come in. Typically, a feature branch is worked on over several days by one developer and when he is done, he submits it to be reviewed by another developer and starts another task while the review is happening. And if something comes up in the review the author needs to switch to the previous work correct the errors he made. And then submit it again for review until it goes through. All this context switching is a major overhead slowing down time to market puts potential value in stock instead of in the hands of our customers.  

[There is more to it than the overhead of context switching](https://thinkinglabs.io/articles/2022/05/30/on-the-evilness-of-feature-branching-the-problems.html). The review is nothing more than a show and almost never catches the real problems. Everything is manual process and is prone to neglect and a lot of human errors. 

## End words 

I have no conclusion on this it’s merely a statement of current affairs and nothing else. 
