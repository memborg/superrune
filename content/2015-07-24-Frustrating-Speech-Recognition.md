+++
title = "Frustrating Speech Recognition"
description = "This must be one of the must frustrating experiences a developer can endurre. I laughed through the entire video when he tries to write code through speech recognition."
template = "page.html"
date = 2015-07-24

[taxonomies]
categories = [ "programming" ]
tags = ["code", "developer"]
+++

This must be one of the must frustrating experiences a developer can endurre.
I laughed through the entire video when he tries to write code through speech
recognition.
<!-- more -->

<iframe width="420" height="315" src="https://www.youtube.com/embed/KyLqUf4cdwc" frameborder="0" allowfullscreen></iframe>

"Thank you"
