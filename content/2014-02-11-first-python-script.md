+++
title = "My first python script"
template = "page.html"
date = 2014-02-11

[taxonomies]
categories = ["programming"]
tags = ["python", "code"]
+++

My first python script! Woohoo to me! I'm working with a file and using
a for-loop. It is crazy. 
<!-- more -->

## The python script 

I needed a list of all my movies in a txt file and I needed an excuse to do
some python. Hold on to you hats, here it comes: 

```python
import os
import glob

path = 'N:\Film\Film'

with open("movies.txt", "a") as myfile:
    for infile in glob.glob(os.path.join(path, '*.*')):
        if ".srt" not in infile:
            myfile.write(infile + "\n")
```

As you can see it is pretty straight forward. 

1.  Open a file with the append parameter
2.  Iterate over the files and foldes in the path. You have the oppotunity to
    do a whitelistning
3.  I've chosen a blacklistning. I do not want my .srt files poluting the end
    result.
4.  If everything adds up, add it to the file as a new line That was pretty
    easy and short to. Soon I will unleash the power of python other projects.
    Happy python'ing
